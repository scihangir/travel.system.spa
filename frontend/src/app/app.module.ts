import {BrowserModule} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { TravelListComponent } from './components/user/travel/travel-list/travel-list.component';
import { TravelDefineComponent } from './components/user/travel/travel-define/travel-define.component';

import {AuthenticationService} from './services/authentication.service';
import {TravelListService} from './services/travel-list.service';
import {TravelDefineService} from './services/travel-define.service';
import {TravelViewService} from './services/travel-view.service';


import {routing} from './app.routing';

import { DatePickerModule } from 'ng2-datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TravelEditComponent } from './components/user/travel/travel-edit/travel-edit.component';
import {TravelRemoveService} from './services/travel-remove.service';
import { NavBarComponent } from './components/user/nav-bar/nav-bar.component';
import {LoginService} from './services/login.service';
import {AuthGuard} from './services/auth.guard';
import {TravelSearchService} from './services/travel-search.service';
import { TravelAllComponent } from './components/admin/travel/travel-all/travel-all.component';
import {UserService} from './services/user.service';
import { UserListComponent } from './components/admin/user-management/user-list/user-list.component';
import { UserDefineComponent } from './components/admin/user-management/user-define/user-define.component';
import { UserEditComponent } from './components/admin/user-management/user-edit/user-edit.component';





@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TravelListComponent,
    TravelDefineComponent,
    TravelEditComponent,
    NavBarComponent,
    TravelAllComponent,
    UserListComponent,
    UserDefineComponent,
    UserEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserModule,
    DatePickerModule,
    BrowserAnimationsModule,
    routing
  ],
  providers: [AuthenticationService, TravelListService, TravelDefineService, TravelViewService,
              TravelRemoveService, TravelSearchService, LoginService, AuthGuard, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
