import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Travel} from '../model/travel';

@Injectable()
export class TravelDefineService {

  constructor(private http: Http) { }

  sendTravel(newTravel: Travel) {
    let url = 'http://localhost:8080/user/saveTravel';
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });
    return this.http.post(url, JSON.stringify(newTravel), {headers: headers} );
  }

}
