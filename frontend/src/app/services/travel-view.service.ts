import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class TravelViewService {

  constructor(private http: Http) { }

  getTravel(id: number) {
    let url = 'http://localhost:8080/user/travel/' + id;
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });
    return this.http.get(url, {headers: headers} );
  }
}
