import { TestBed, inject } from '@angular/core/testing';

import { TravelSearchService } from './travel-search.service';

describe('TravelSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TravelSearchService]
    });
  });

  it('should be created', inject([TravelSearchService], (service: TravelSearchService) => {
    expect(service).toBeTruthy();
  }));
});
