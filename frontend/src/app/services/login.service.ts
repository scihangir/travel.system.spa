import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class LoginService {

  constructor(private http: Http) { }

  getActiveUser() {
    let url = 'http://localhost:8080/user  ';
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });
    return this.http.get(url, {headers: headers });
  }



}
