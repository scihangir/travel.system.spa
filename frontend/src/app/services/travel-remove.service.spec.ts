import { TestBed, inject } from '@angular/core/testing';

import { TravelRemoveService } from './travel-remove.service';

describe('TravelRemoveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TravelRemoveService]
    });
  });

  it('should be created', inject([TravelRemoveService], (service: TravelRemoveService) => {
    expect(service).toBeTruthy();
  }));
});
