import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Travel} from '../model/travel';

@Injectable()
export class TravelRemoveService {

  constructor(private http: Http) { }

  deleteTravel(travel: Travel) {
    let url = 'http://localhost:8080/user/travel/remove';
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });

    return this.http.post(url, travel.id, {headers: headers });
  }

}
