import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';


@Injectable()
export class TravelListService {

  constructor(private http: Http) { }

  getTravelList() {
    let url = 'http://localhost:8080/user/travelList  ';
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });
    return this.http.get(url, {headers: headers });
  }

  getAllTravel() {
    let url = 'http://localhost:8080/admin/travelList  ';
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });
    return this.http.get(url, {headers: headers });
  }

}
