import { TestBed, inject } from '@angular/core/testing';

import { TravelDefineService } from './travel-define.service';

describe('TravelDefineService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TravelDefineService]
    });
  });

  it('should be created', inject([TravelDefineService], (service: TravelDefineService) => {
    expect(service).toBeTruthy();
  }));
});
