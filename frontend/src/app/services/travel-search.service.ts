import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class TravelSearchService {

  constructor(private http: Http) { }

  searchTravel(sDate: Date, eDate: Date) {
    let url = 'http://localhost:8080/user/travel/search/' + sDate + '/' + eDate;
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });

    return this.http.get(url, {headers: headers });
  }

  searchTravelAdmin(sDate: Date, eDate: Date) {
    let url = 'http://localhost:8080/admin/travel/search/' + sDate + '/' + eDate;
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });

    return this.http.get(url, {headers: headers });
  }
}
