import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {JwtUser} from '../model/jwtuser';

@Injectable()
export class UserService {

  constructor(private http: Http) { }

  getAllUser() {
    let url = 'http://localhost:8080/admin/usermanagement  ';
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });
    return this.http.get(url, {headers: headers });
  }

  getTravelByUser(userId: number) {
    console.log('Servise gelen Id:' + userId)
    let url = 'http://localhost:8080/admin/travelByUser/' + userId;
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });
    return this.http.get(url, {headers: headers });
  }

  sendUser(newUser: JwtUser) {
    let url = 'http://localhost:8080/admin/saveUser';
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });
    return this.http.post(url, JSON.stringify(newUser), {headers: headers} );
  }

  getUser(id: number) {
    let url = 'http://localhost:8080/admin/user/' + id;
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });
    return this.http.get(url, {headers: headers} );
  }


  deleteUser(user: JwtUser) {
    let url = 'http://localhost:8080/admin/user/remove';
    const headers = new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      'Authorization' : localStorage.getItem('jwtToken')
    });

    return this.http.post(url, user.id, {headers: headers });
  }
}
