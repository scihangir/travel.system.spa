import { TestBed, inject } from '@angular/core/testing';

import { TravelViewService } from './travel-view.service';

describe('TravelViewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TravelViewService]
    });
  });

  it('should be created', inject([TravelViewService], (service: TravelViewService) => {
    expect(service).toBeTruthy();
  }));
});
