import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class AuthenticationService {

  public token: string;

  constructor(private http: Http) {
  }

  login(username: string, password: string) {

    let url = 'http://localhost:8080/auth';
    const headers = new Headers({ 'Content-Type': 'application/json' });

    return this.http.post(url, JSON.stringify({username: username, password: password}), {headers: headers} );
  }






}
