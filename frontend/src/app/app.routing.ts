/**
 * Created by cihangir on 8/18/17.
 */


import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {TravelListComponent} from './components/user/travel/travel-list/travel-list.component';
import {TravelDefineComponent} from './components/user/travel/travel-define/travel-define.component';
import {TravelEditComponent} from './components/user/travel/travel-edit/travel-edit.component';
import {AuthGuard} from './services/auth.guard';
import {TravelAllComponent} from './components/admin/travel/travel-all/travel-all.component';
import {UserListComponent} from './components/admin/user-management/user-list/user-list.component';
import {UserDefineComponent} from './components/admin/user-management/user-define/user-define.component';
import {UserEditComponent} from './components/admin/user-management/user-edit/user-edit.component';



const appRoutes: Routes = [
  {
    path : '',
    component: LoginComponent,
  },
  {
    path: 'user/travelList',
    component: TravelListComponent,
    canActivate: [AuthGuard]

  },
  {
    path: 'user/travelDefine',
    component: TravelDefineComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'user/editTravel/:id',
    component: TravelEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/travelList',
    component: TravelAllComponent
  },
  {
    path: 'admin/userList',
    component: UserListComponent
  },
  {
    path: 'admin/userDefine',
    component: UserDefineComponent
  },
  {
    path: 'admin/editUser/:id',
    component: UserEditComponent
  },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
