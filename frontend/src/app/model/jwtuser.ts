/**
 * Created by cihangir on 8/29/17.
 */


export class JwtUser {

  id: number;
  username: string;
  password: string;
  registrationNumber: string;
  department: string;
  departmentHeader: string;
  informing: string;
  period: string;
  dayOfPeriod: string;


}
