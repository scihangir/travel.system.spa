/**
 * Created by cihangir on 8/20/17.
 */

export class Travel {

  id: number;
  destination: string;
  startDate: Date;
  endDate: Date;
  purpose: string;
  projectCode: string;
  cost: string;

}
