import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {}

  constructor(private authenticationService: AuthenticationService,
              private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.authenticationService.login(this.model.username, this.model.password).subscribe(
      res => {
        localStorage.setItem('jwtToken', res.json().token);
        if ( res.json().grantedAuthority.authority === 'ROLE_ADMIN') {
          localStorage.setItem('role', 'ROLE_ADMIN');
          this.router.navigate(['/admin/travelList']);
        }else if ( res.json().grantedAuthority.authority === 'ROLE_USER') {
          localStorage.setItem('role', 'ROLE_USER');
          this.router.navigate(['/user/travelList']);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

}
