import { Component, OnInit } from '@angular/core';
import {TravelDefineService} from '../../../../services/travel-define.service';
import {Router} from '@angular/router';
import {Travel} from '../../../../model/travel';

@Component({
  selector: 'app-travel-define',
  templateUrl: './travel-define.component.html',
  styleUrls: ['./travel-define.component.css']
})
export class TravelDefineComponent implements OnInit {

  private newTravel: Travel = new Travel();



  constructor(private travelDefineService: TravelDefineService, private router: Router) {}

  onSubmit() {
    this.travelDefineService.sendTravel(this.newTravel).subscribe(
      res => {
        this.router.navigate(['/user/travelList']);
      },
      error => {
        console.log(error);
      }
    );
  }


  ngOnInit() {
  }

}
