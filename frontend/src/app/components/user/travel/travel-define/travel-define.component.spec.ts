import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelDefineComponent } from './travel-define.component';

describe('TravelDefineComponent', () => {
  let component: TravelDefineComponent;
  let fixture: ComponentFixture<TravelDefineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TravelDefineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelDefineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
