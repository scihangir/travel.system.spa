import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {TravelListService} from '../../../../services/travel-list.service';
import {Travel} from '../../../../model/travel';
import {TravelRemoveService} from '../../../../services/travel-remove.service';
import {TravelSearchService} from '../../../../services/travel-search.service';


@Component({
  selector: 'app-travel-list',
  templateUrl: './travel-list.component.html',
  styleUrls: ['./travel-list.component.css']
})
export class TravelListComponent implements OnInit {


  travelList: any [];
  sDate: Date;
  eDate: Date;

  constructor(
    private travelListService: TravelListService,
    private travelRemoveService: TravelRemoveService,
    private travelSearchService: TravelSearchService,
    private router: Router
  ) { }


  getTravelList() {
    this.travelListService.getTravelList().subscribe(
      res => {
        this.travelList = res.json();
      },
      error => {
        console.log(error);
      }
    );
  }

  onDelete(travel: Travel) {
    this.travelRemoveService.deleteTravel(travel).subscribe(
      res => {
        this.getTravelList();
      },
      err => {
        console.log(err);
      }
    );
  }

  onSearch() {
    this.travelSearchService.searchTravel(this.sDate, this.eDate).subscribe(
      res => {
        this.travelList = res.json();
      },
      err => {
        console.log(err);
      }
    );
  }



  ngOnInit() {
    this.getTravelList();
  }

}
