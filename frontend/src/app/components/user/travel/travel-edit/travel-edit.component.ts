import { Component, OnInit } from '@angular/core';
import {Travel} from '../../../../model/travel';
import {TravelDefineService} from '../../../../services/travel-define.service';
import {TravelViewService} from '../../../../services/travel-view.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-travel-edit',
  templateUrl: './travel-edit.component.html',
  styleUrls: ['./travel-edit.component.css']
})
export class TravelEditComponent implements OnInit {

  private travelId: number;
  private newTravel: Travel = new Travel();
  constructor(
    private travelDefineService: TravelDefineService,
    private travelViewService: TravelViewService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }


    onSubmit() {
      this.travelDefineService.sendTravel(this.newTravel).subscribe(
        res => {
          this.router.navigate(['/user/travelList']);
        },
        error => {
          console.log(error);
        }
      );
     }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      (params: Params) => {
        this.travelId = params['id'];
      }
    );

    this.travelViewService.getTravel(this.travelId).subscribe(
      res => {
        this.newTravel = res.json();
      },
      error => console.log(error)
    );
  }

}
