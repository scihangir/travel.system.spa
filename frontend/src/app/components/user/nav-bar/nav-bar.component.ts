import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../services/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  username: string;

  constructor(private loginService: LoginService, private router: Router) { }

  activeUser () {
    this.loginService.getActiveUser().subscribe(
      res => {
            this.username = res.json().username;
      },
      error => {
        console.log(error);
      }
    );
  }

  onLogout() {
    localStorage.removeItem('jwtToken');
    localStorage.removeItem('role');
    this.router.navigate(['/']);
  }

  ngOnInit() {
    this.activeUser();
  }

}
