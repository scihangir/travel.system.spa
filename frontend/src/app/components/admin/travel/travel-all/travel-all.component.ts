import { Component, OnInit } from '@angular/core';
import {TravelListService} from '../../../../services/travel-list.service';
import {Router} from '@angular/router';
import {UserService} from '../../../../services/user.service';
import {TravelSearchService} from '../../../../services/travel-search.service';

@Component({
  selector: 'app-travel-all',
  templateUrl: './travel-all.component.html',
  styleUrls: ['./travel-all.component.css']
})
export class TravelAllComponent implements OnInit {

  travelList: any [];
  userList: any [];
  sDate: Date;
  eDate: Date;


  constructor( private travelListService: TravelListService,
               private userService: UserService,
               private travelSearchService: TravelSearchService,
               private router: Router) { }

  getAllTravel() {
    this.travelListService.getAllTravel().subscribe(
      res => {
        this.travelList = res.json();
      },
      error => {
        console.log(error);
      }
    );
  }

  onSearch() {
    this.travelSearchService.searchTravelAdmin(this.sDate, this.eDate).subscribe(
      res => {
        this.travelList = res.json();
      },
      err => {
        console.log(err);
      }
    );
  }

  getAllUser() {
    this.userService.getAllUser().subscribe(
      res => {
        this.userList = res.json();
      },
      error => {
        console.log(error);
      }
    );
  }



  onChange(userId: number) {
    this.userService.getTravelByUser(userId).subscribe(
      res => {
        this.travelList = res.json();
      },
      err => {
        console.log(err);
      }
    );
  }

  ngOnInit() {
    this.getAllUser();
    this.getAllTravel();
  }

}
