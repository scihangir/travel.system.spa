import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TravelAllComponent } from './travel-all.component';

describe('TravelAllComponent', () => {
  let component: TravelAllComponent;
  let fixture: ComponentFixture<TravelAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TravelAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
