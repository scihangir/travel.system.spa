import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../../services/user.service';
import {Router} from '@angular/router';
import {JwtUser} from '../../../../model/jwtuser';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  userList: any [];


  constructor(private userService: UserService, private router: Router) { }

  getUserList() {
    this.userService.getAllUser().subscribe(
      res => {
        this.userList = res.json();
      },
      error => {
        console.log(error);
      }
    );
  }

  onDelete(user: JwtUser) {
    this.userService.deleteUser(user).subscribe(
      res => {
        this.getUserList();
      },
      err => {
        console.log(err);
      }
    );
  }

  ngOnInit() {
    this.getUserList();
  }

}
