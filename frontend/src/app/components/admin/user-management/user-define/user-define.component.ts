import { Component, OnInit } from '@angular/core';
import {JwtUser} from '../../../../model/jwtuser';
import {UserService} from '../../../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-define',
  templateUrl: './user-define.component.html',
  styleUrls: ['./user-define.component.css']
})
export class UserDefineComponent implements OnInit {


  private newUser: JwtUser = new JwtUser();

  constructor(private userService: UserService, private router: Router) { }

  onSubmit() {
    this.userService.sendUser(this.newUser).subscribe(
      res => {
        this.router.navigate(['/admin/userList']);
      },
      error => {
        console.log(error);
      }
    );
  }


  ngOnInit() {
  }

}
