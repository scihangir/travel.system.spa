import { Component, OnInit } from '@angular/core';
import {JwtUser} from '../../../../model/jwtuser';
import {UserService} from '../../../../services/user.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  private userId: number;
  private newUser: JwtUser = new JwtUser();

  constructor(private userService: UserService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  onSubmit() {
    this.userService.sendUser(this.newUser).subscribe(
      res => {
        this.router.navigate(['/admin/userList']);
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      (params: Params) => {
        this.userId = params['id'];
      }
    );

    this.userService.getUser(this.userId).subscribe(
      res => {
        this.newUser = res.json();
      },
      error => console.log(error)
    );
  }
}
