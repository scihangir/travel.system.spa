package com.cihangir.service;

import com.cihangir.model.Travel;
import com.cihangir.model.User;

import java.util.Date;
import java.util.List;

/**
 * Created by cihangir on 8/20/17.
 */
public interface TravelService {

    List<Travel> getTravelByUser(User user);

    Travel saveTravel(Travel theTravel);

    Travel findTravelById(Long theId);

    void deleteTravel(Long theId);

    List<Travel> getTravelsBetweenDateByUserId(Date d1, Date d2, Long id);

    List<Travel> findAllTravels();

    List<Travel> getTravelsBetweenDates(Date d1, Date d2);

    void deleteTravelByUserId(Long theId);

    List<Travel> getTravelByUserId(Long id);
}
