package com.cihangir.service.impl;

import com.cihangir.dao.UserRepository;
import com.cihangir.model.User;
import com.cihangir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cihangir on 8/20/17.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;



    @Override
    public User findByName(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> getAllUser() {
        List<User> userList = new ArrayList<>();

        for(User user : userRepository.findAll()){
            userList.add(user);
        }

        return userList;
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.findUserById(id);
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(Long theId) {
        userRepository.delete(theId);
    }

    @Override
    public List<User> getUserByRole(String role) {
        return userRepository.findUserByRole(role);
    }
}
