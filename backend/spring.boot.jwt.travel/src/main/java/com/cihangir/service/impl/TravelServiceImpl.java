package com.cihangir.service.impl;

import com.cihangir.dao.TravelRepository;
import com.cihangir.model.Travel;
import com.cihangir.model.User;
import com.cihangir.service.TravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by cihangir on 8/20/17.
 */

@Service
@Transactional
public class TravelServiceImpl implements TravelService {

    @Autowired
    private TravelRepository travelRepository;


    @Override
    public List<Travel> getTravelByUser(User user) {
        return travelRepository.findTravelByUser(user);
    }

    @Override
    public Travel saveTravel(Travel theTravel) {
        return travelRepository.save(theTravel);
    }

    @Override
    public Travel findTravelById(Long theId) {
        return travelRepository.findOne(theId);
    }

    @Override
    public void deleteTravel(Long theId) {
        travelRepository.delete(theId);
    }

    @Override
    public List<Travel> getTravelsBetweenDateByUserId(Date d1, Date d2, Long id) {
        return travelRepository.findByUserAndDates(id,d1,d2);
    }

    @Override
    public List<Travel> findAllTravels() {
        List<Travel> travelList = new ArrayList<>();

        for(Travel travel : travelRepository.findAll()){
            travelList.add(travel);
        }

        return travelList;
    }

    @Override
    public List<Travel> getTravelsBetweenDates(Date d1, Date d2) {
        return travelRepository.findTravelsBetweenDate(d1, d2);
    }



    @Override
    public void deleteTravelByUserId(Long theId) {

    }

    @Override
    public List<Travel> getTravelByUserId(Long id) {
        return travelRepository.findTravelByUser_id(id);
    }
}
