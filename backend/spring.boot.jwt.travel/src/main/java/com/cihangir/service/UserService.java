package com.cihangir.service;

import com.cihangir.model.User;

import java.util.List;

/**
 * Created by cihangir on 8/20/17.
 */
public interface UserService {
    public User findByName(String username);
    public List<User> getAllUser();
    public User findUserById(Long id);
    public User saveUser(User user);
    public void deleteUser(Long theId);
    public List<User> getUserByRole(String role);
}
