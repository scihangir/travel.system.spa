package com.cihangir.resource;

import com.cihangir.model.User;
import com.cihangir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class HomeController {

	@Autowired
	private UserService userService;
	
	@RequestMapping("/")
	public String home() {
		return "Hello World!!!";
	}

	@RequestMapping("/user")
	public User user(Principal principal) {
		User user = new User();
		if (null != principal) {
			user = userService.findByName(principal.getName());
		}

		return user;
	}

}
