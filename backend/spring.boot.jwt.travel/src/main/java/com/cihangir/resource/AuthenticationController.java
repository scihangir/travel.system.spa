package com.cihangir.resource;

import com.cihangir.model.Travel;
import com.cihangir.model.User;
import com.cihangir.security.model.AuthenticationRequest;
import com.cihangir.security.model.AuthenticationResponse;
import com.cihangir.security.model.JwtUser;
import com.cihangir.security.util.JwtTokenUtil;
import com.cihangir.service.TravelService;
import com.cihangir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cihangir on 8/18/17.
 */

@RestController
public class AuthenticationController {


    @Autowired
    private TravelService travelService;

    @Autowired
    private UserService userService;

    @Value("Authorization")
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @RequestMapping(value = "auth", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest, Device device) throws AuthenticationException {

        // Perform the security
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Reload password post-security so we can generate token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails, device);


        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>();

        for (GrantedAuthority grantedAuthority: userDetails.getAuthorities()) {
            Result.add(grantedAuthority);
        }


        // Return the token
        return ResponseEntity.ok(new AuthenticationResponse(token, Result.get(0)));
    }

    @RequestMapping(value = "refresh", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);

        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>();

        for (GrantedAuthority grantedAuthority: user.getAuthorities()) {
            Result.add(grantedAuthority);
        }

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            return ResponseEntity.ok(new AuthenticationResponse(refreshedToken, Result.get(0)));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

}
