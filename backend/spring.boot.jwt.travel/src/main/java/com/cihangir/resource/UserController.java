package com.cihangir.resource;

import com.cihangir.model.Travel;
import com.cihangir.model.User;
import com.cihangir.security.util.JwtTokenUtil;
import com.cihangir.service.TravelService;
import com.cihangir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by cihangir on 8/20/17.
 */


@RestController
@PreAuthorize("hasRole('ROLE_USER')")
public class UserController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private TravelService travelService;





    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> getUserList() {
        return userService.getAllUser();
    }


    @RequestMapping(value = "/user/travelList", method = RequestMethod.GET)
    public List<Travel> getTravelList(Principal principal) {

        User user = userService.findByName(principal.getName());

        // get travels from the service
        List<Travel> travelList=travelService.getTravelByUser(user);

        return travelList;
    }
    @RequestMapping(value = "/user/saveTravel", method = RequestMethod.POST)
    public Travel saveTravel(Principal principal, @RequestBody Travel  theTravel) {

        User theUser = userService.findByName(principal.getName());
        theTravel.setUser(theUser);


        return travelService.saveTravel(theTravel);
    }

    @RequestMapping("/user/travel/{id}")
    public Travel getTravel(@PathVariable("id") Long id){
        Travel travel = travelService.findTravelById(id);
        return travel;
    }

    @RequestMapping(value="/user/travel/remove", method=RequestMethod.POST)
    public ResponseEntity remove(@RequestBody String id) throws IOException {
        travelService.deleteTravel(Long.parseLong(id));
        return new ResponseEntity("Remove Success!", HttpStatus.OK);
    }

    @RequestMapping("/user/travel/search/{sDate}/{eDate}")
    public List<Travel> search(Principal principal, @PathVariable("sDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date d1, @PathVariable("eDate")  @DateTimeFormat(pattern="yyyy-MM-dd")  Date d2) {

        User theUser = userService.findByName(principal.getName());

        return travelService.getTravelsBetweenDateByUserId(d1, d2, theUser.getId());
    }

}
