package com.cihangir.resource;

import com.cihangir.model.Travel;
import com.cihangir.model.User;
import com.cihangir.service.TravelService;
import com.cihangir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * Created by cihangir on 8/18/17.
 */

@RestController
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private TravelService travelService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping("/admin")
    public String hello() {
        return "Hello Admin";
    }

    @RequestMapping(value = "/admin/travelList", method = RequestMethod.GET)
    public List<Travel> getTravelList() {

        // get travels from the service
        List<Travel> travelList=travelService.findAllTravels();

        return travelList;
    }

    @RequestMapping("/admin/user/{id}")
    public User getTravel(@PathVariable("id") Long id){
        User user = userService.findUserById(id);;


        return user;
    }

    @RequestMapping(value = "/admin/saveUser", method = RequestMethod.POST)
    public User saveTravel(@RequestBody User  theUser) {

        theUser.setRole("ROLE_USER");
        theUser.setPassword(passwordEncoder.encode(theUser.getPassword()));
        theUser.setLastPasswordResetDate(new Date());

        return userService.saveUser(theUser);
    }

    @RequestMapping("/admin/usermanagement")
    public  List<User> getUserList() {
        List<User> userList=userService.getUserByRole("ROLE_USER");

        return userList;
    }

    @RequestMapping("/admin/travelByUser/{userId}")
    public  List<Travel> getTravelByUser(@PathVariable("userId") Long id) {

        List<Travel> travelList=travelService.getTravelByUserId(id);

        return travelList;
    }

    @RequestMapping("/admin/travel/search/{sDate}/{eDate}")
    public List<Travel> search(@PathVariable("sDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date d1, @PathVariable("eDate")  @DateTimeFormat(pattern="yyyy-MM-dd")  Date d2) {
        return travelService.getTravelsBetweenDates(d1,d2);
    }

    @RequestMapping(value="/admin/user/remove", method=RequestMethod.POST)
    public ResponseEntity remove(@RequestBody Long id) throws IOException {
        userService.deleteUser(id);
        return new ResponseEntity("Remove Success!", HttpStatus.OK);
    }

}
