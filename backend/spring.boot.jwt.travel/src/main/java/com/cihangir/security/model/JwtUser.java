package com.cihangir.security.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by cihangir on 8/18/17.
 */
public class JwtUser implements UserDetails {

    private Long id;
    private String username;
    private String password;
    private String registrationNumber;
    private String department;
    private String departmentHeader;
    private String informing;
    private String period;
    private String dayOfPeriod;
    private Date lastPasswordResetDate;
    private Collection<? extends GrantedAuthority> authorities;


    public JwtUser(
            Long id,
            String username,
            String password,
            String registrationNumber,
            String department,
            String departmentHeader,
            String informing,
            String period,
            String dayOfPeriod,
            Date lastPasswordResetDate,
            Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.registrationNumber = registrationNumber;
        this.department = department;
        this.departmentHeader = departmentHeader;
        this.informing = informing;
        this.period = period;
        this.dayOfPeriod = dayOfPeriod;
        this.lastPasswordResetDate = lastPasswordResetDate;
        this.authorities = authorities;
    }

    public JwtUser(Long id, String username, String password, String registrationNumber, String department, String departmentHeader, String informing, String period, String dayOfPeriod, List<GrantedAuthority> grantedAuthorities, Date lastPasswordResetDate) {
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    @JsonIgnore
    @Override
    public String getUsername() {
        return username;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @JsonIgnore
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    @JsonIgnore
    public String getDepartment() {
        return department;
    }

    @JsonIgnore
    public String getDepartmentHeader() {
        return departmentHeader;
    }

    @JsonIgnore
    public String getInforming() {
        return informing;
    }

    @JsonIgnore
    public String getPeriod() {
        return period;
    }

    @JsonIgnore
    public String getDayOfPeriod() {
        return dayOfPeriod;
    }

    @JsonIgnore
    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }
}
