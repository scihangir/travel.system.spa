package com.cihangir.security.factory;

import com.cihangir.model.User;
import com.cihangir.security.model.JwtUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cihangir on 8/18/17.
 */
public final class JwtUserFactory {
    private JwtUserFactory() {
    }

    public static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getRegistrationNumber(),
                user.getDepartment(),
                user.getDepartmentHeader(),
                user.getInforming(),
                user.getPeriod(),
                user.getDayOfPeriod(),
                user.getLastPasswordResetDate(),
                buildUserAuthority(user.getRole())
        );
    }

    private static List<GrantedAuthority> buildUserAuthority(String authority) {
        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>();
        Result.add(new SimpleGrantedAuthority(authority));

        return Result;
    }
}
