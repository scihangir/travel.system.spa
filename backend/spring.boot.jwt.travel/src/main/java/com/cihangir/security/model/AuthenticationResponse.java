package com.cihangir.security.model;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by cihangir on 8/18/17.
 */
public class AuthenticationResponse {

    private final String token;
    private final GrantedAuthority grantedAuthority;

    public AuthenticationResponse(String token, GrantedAuthority grantedAuthority) {
        this.token = token;
        this.grantedAuthority=grantedAuthority;
    }

    public String getToken() {
        return this.token;
    }

    public GrantedAuthority getGrantedAuthority() {
        return grantedAuthority;
    }
}
