package com.cihangir.dao;

import com.cihangir.model.Travel;
import com.cihangir.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by cihangir on 8/18/17.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);

    List<User> findUserByRole(String role);

    User findUserById(Long id);

}
