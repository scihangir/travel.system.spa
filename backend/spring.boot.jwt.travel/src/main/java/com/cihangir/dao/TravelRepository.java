package com.cihangir.dao;

import com.cihangir.model.Travel;
import com.cihangir.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

/**
 * Created by cihangir on 8/20/17.
 */
public interface TravelRepository extends CrudRepository<Travel, Long> {

    List<Travel> findTravelByUser(User user);



    @Query("select b from Travel b " +
            "where b.startDate between ?1 and ?2 and b.endDate between ?1 and ?2")
    List<Travel> findTravelsBetweenDate(Date d1, Date d2);

    List<Travel> findTravelByUser_id(Long user_id);



    @Query("SELECT t FROM Travel t WHERE t.user.id = :id AND t.startDate between :d1 and :d2 AND t.endDate between :d1 and :d2")
    List<Travel> findByUserAndDates(@Param("id") Long id,
                                    @Param("d1") Date d1,
                                    @Param("d2") Date d2);




}
