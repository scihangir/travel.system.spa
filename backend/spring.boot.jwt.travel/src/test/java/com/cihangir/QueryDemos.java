package com.cihangir;

import com.cihangir.dao.TravelRepository;
import com.cihangir.dao.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by cihangir on 8/30/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class QueryDemos {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TravelRepository travelRepository;

    @Test
    public void jpqlQueries() {

        System.out.println("Find User By Id\n");
        System.out.println(userRepository.findUserById(new Long(2)));



//        //*******Method Simplification*******
//
//        System.out.println("Find Courses where Jones is the department Chair with Property Expression");
//        courseRepository.findByDepartmentChairMemberLastName("Jones").forEach(System.out::println);
//
//        //Select c from Course c where c.department.chair.member.lastName=:chair
//        System.out.println("\nFind Courses where Jones is the department Chair with @Query");
//        courseRepository.findByChairLastName("Jones").forEach(System.out::println);
//
//
//        //*******Complex Queries********
//        Course english101 = courseRepository.findByName("English 101");
//
//        //Select c from Course c join c.prerequisites p where p.id = ?1
//        System.out.println("\nFind Courses where English 101 is a prerequisite");
//        courseRepository.findCourseByPrerequisite(english101.getId())
//                .forEach(System.out::println);
//
//        //Select new com.example.university.view.CourseView
//        //  (c.name, c.instructor.member.lastName, c.department.name) from Course c where c.id=?1
//        System.out.println("\nCourseView for English 101 \n" +
//                courseRepository.getCourseView(english101.getId()));
    }

    @Before
    @After
    public void printBanner() {
        System.out.println("*************************************************************************************");
    }

}
